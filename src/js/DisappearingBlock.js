import {Block} from "./Block";

export class DisappearingBlock extends Block {
    constructor(props) {
        super(props);

        this.color = 'white';
        this._actionType = 'delete'
    }
}