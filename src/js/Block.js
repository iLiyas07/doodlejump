import {BaseClass} from "./BaseClass";
import {Vec2} from "./lib";

export class Block extends BaseClass {
    constructor(opt) {
        super();

        this.pos = new Vec2(opt.pos.x, opt.pos.y);
        this.size = new Vec2(opt.size.x, opt.size.y);
        this.color = 'black';
        this._actionType = 'nothing';
    }
    get actionType() {
        return this._actionType
    }
    nextTick(dt) {

    }
    render(ctx, dy) {
        const width = this.size.x;
        ctx.fillStyle = this.color;
        ctx.fillRect(
            (this.pos.x - width / 2) * this.scale, ctx.canvas.height - this.pos.y * this.scale + dy,
            width * this.scale, this.size.y * this.scale)
    }
}