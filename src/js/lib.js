export class Vec2 {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    sum(vec) {
        this.x += vec.x;
        this.y += vec.y;
    }
}

export class Vec3 extends Vec2 {
    constructor(x, y, z) {
        super(x, y);
        this.z = z;
    }
}

export function applyIncrement(f, ff, dt) {
    f.x += ff.x * dt / 1000;
    f.y += ff.y * dt / 1000;
}

export function randomOf(arr) {
    return arr[Math.floor(Math.random() * arr.length)]
}

export function randomOfProbability(arr) {
    let sumP = 0;
    arr.forEach((el)=> {
        sumP += el.p;
    });
    let currentSumP = 0;
    const random = Math.random() * sumP;
    for (let i = 0; i < arr.length; i++) {
        currentSumP += arr[i].p;
        if (currentSumP >= random) {
            return arr[i].el;
        }
    }

}

export function getDirection(point1, point2) {
    return (point2 - point1) >= 0 ? 1 : -1;
}

export function vec2Sum() {
    const arr = [...arguments];
    const sum = new Vec2(0, 0);
    arr.forEach((vec)=> {
        sum.x += vec.x;
        sum.y += vec.y;
    });
    return sum;
}

export function vec2Multiply() {
    const arr = [...arguments];
    const multiply = new Vec2(1, 1);
    arr.forEach((vec)=> {
        multiply.x *= vec.x;
        multiply.y *= vec.y;
    });
    return multiply;
}

export function getProbabilityObj(el, p) {
    return {el, p};
}