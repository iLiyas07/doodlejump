import './index.less';

import {Game} from "./js/Game";

const canvas = document.querySelector('#canvas');
const ctx = canvas.getContext('2d');

const game = new Game(canvas);

void function render() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    game.render(ctx);
    requestAnimationFrame(render);
}();

void function nextTick() {
    game.nextTick();
    setTimeout(nextTick, 5);
}();