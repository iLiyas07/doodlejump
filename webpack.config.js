const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const IgnoreEmitPlugin = require('ignore-emit-webpack-plugin');
const autoprefixer = require('autoprefixer');
const ZipPlugin = require('zip-webpack-plugin');
const TinyPngWebpackPlugin = require('tinypng-webpack-plugin');

module.exports = (env) => {
    const isProduction = env && env.production === true;
    const inlineFilesRegexp = /banner\.(js|css)$/;

    return {
        entry: {
            banner: './src/index.js'
        },
        output: {
            filename: '[name].js',
            path: path.resolve(__dirname, 'dist')
        },
        devtool: isProduction ? false : 'source-map',
        mode: isProduction ? 'production' : 'development',
        optimization: {
            minimizer: [
                new TerserJSPlugin({
                    terserOptions: {
                        output: {
                            comments: false
                        }
                    }
                }),
                new OptimizeCSSAssetsPlugin({})
            ],
            splitChunks: {
                cacheGroups: {
                    vendor: {
                        test: /\/node_modules\//,
                        name: 'vendor',
                        chunks: 'all'
                    }
                }
            }
        },
        devServer: {
            open: true
        },
        module: {
            rules: [
                {
                    test: /\.html$/,
                    loader: 'html-loader'
                },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                },
                {
                    test: /\.less$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: !isProduction
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: isProduction ? [
                                    autoprefixer()
                                ] : [],
                                sourceMap: !isProduction
                            }
                        },
                        {
                            loader: 'less-loader',
                            options: {
                                sourceMap: !isProduction
                            }
                        }
                    ]
                },
                {
                    test: /\.(png|jpe?g|gif)(\?.*)?$/,
                    loader: 'file-loader',
                    options: {
                        name: 'images/[name].[ext]'
                    }
                },
                {
                    test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[name].[ext]'
                    }
                },
                {
                    test: /\.svg$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: 'images/[name].[ext]'
                            }
                        },
                        'svgo-loader'
                    ]
                }
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: 'src/index.html',
                inlineSource: inlineFilesRegexp,
                minify: isProduction ? {
                    html5: true,
                    removeEmptyAttributes: true,
                    removeComments: true,
                    collapseWhitespace: true,
                    conservativeCollapse: true
                } : false
            }),
            new MiniCssExtractPlugin({
                filename: 'banner.css'
            }),
            ...isProduction
                ? [
                    new CleanWebpackPlugin(),
                    new HtmlWebpackInlineSourcePlugin(),
                    new IgnoreEmitPlugin(inlineFilesRegexp),
                    new TinyPngWebpackPlugin({
                        key: 'JYvQ6XRpXXtuQIP2RuhvLwvCZDuwieAq'
                    }),
                    new ZipPlugin({
                        filename: path.basename(__dirname) + '.zip'
                    })
                ]
                : []
        ]
    };
};
