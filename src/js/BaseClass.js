export class BaseClass {
    constructor() {
        this.scale = 1;
        this._items = {
            blocks: [],
            player: [],
        };
        window.addEventListener('resize', this.updateScale.bind(this));
        this.updateScale();
    }
    updateScale() {
        this.scale = window.innerHeight / 820 * 200;
    }
    get items(){
        return this._items
    }
    render(ctx, dy) {
        for (const key in this.items) {
            this.items[key].forEach((item)=> {
                item.render(ctx, dy);
            })
        }
    }
    nextTick(dt) {
        for (const key in this.items) {
            this.items[key].forEach((item)=> {
                item.nextTick(dt);
            })
        }
    }
}