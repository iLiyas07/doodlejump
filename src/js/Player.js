import {BaseClass} from "./BaseClass";
import {Vec2, applyIncrement} from "./lib";

const playerMass = 1; // kg
const gravity = new Vec2(0, -9.8);
const jumpImpulse = 5.5;
const speedLimit = 3;

export class Player extends BaseClass{
    constructor(opt) {
        super();
        this.pos = opt.pos;
        this.prevPos = new Vec2();
        this.size = opt.size;
        this.worldSize = opt.worldSize;

        this.mass = playerMass;
        this.speed = new Vec2(0, 0);
        this.gravity = new Vec2(gravity.x, gravity.y);
        this.a = new Vec2(this.gravity.x, this.gravity.y);

        this.isOnSurface = true;

        this.actions = [];

        this.keydownhandlers = {
            '32': this.jump.bind(this),
            '39': this.goRight.bind(this),
            '37': this.goLeft.bind(this),
        };
        this.keyuphandlers = {
            '39': this.stopRight.bind(this),
            '37': this.stopLeft.bind(this),
        };

        this.walkingStatus = {
            left: false,
            right: false,
        };
        window.a = this.walkingStatus;

        document.addEventListener('keydown', this.keydownHandler.bind(this))
        document.addEventListener('keyup', this.keyupHandler.bind(this))
    }
    applyImpulse(impulse) {
        this.speed.x += impulse.x / this.mass;
        this.speed.y += impulse.y / this.mass;
    }
    // applyForce(dt) { // TODO дописать applyForce()
    //
    // }
    keydownHandler(e) {
        if (this.keydownhandlers[e.keyCode])
        this.actions.push(this.keydownhandlers[e.keyCode]);
    }
    keyupHandler(e) {
        if (this.keyuphandlers[e.keyCode])
            this.actions.push(this.keyuphandlers[e.keyCode]);
    }
    jump() {
        if (!this.isOnSurface) return;
        this.applyImpulse(new Vec2(0, jumpImpulse));
    }
    goLeft() {
        this.walkingStatus.left = true;
        this.walkingStatus.right = false;
    }
    goRight() {
        this.walkingStatus.right = true;
        this.walkingStatus.left = false;
    }
    stopLeft() {
        this.walkingStatus.left = false;
    }
    stopRight() {
        this.walkingStatus.right = false;
    }
    walking() { // TODO переписать через applyForce()
        if (!this.walkingStatus.left && !this.walkingStatus.right) {
            this.speed.x *= .99;
        }
        if (this.walkingStatus.left) {
            this.speed.x -= speedLimit / 50;
            this.speed.x = Math.max(-speedLimit, this.speed.x);
        } else if (this.walkingStatus.right) {
            this.speed.x += speedLimit / 50;
            this.speed.x = Math.min(speedLimit, this.speed.x);
        }
    }
    collisionHandler() {
        if (this.pos.x + this.size.x / 2 < 0) {
            this.pos.x = this.worldSize.x + this.size.x / 2;
        } else if (this.pos.x > this.worldSize.x + this.size.x / 2) {
            this.pos.x = -this.size.x / 2;
        }
        if (this.isOnSurface && !this.walkingStatus.left && !this.walkingStatus.right) {
            this.speed.x *= .96;
        }
    }
    nextTick(dt) {
        this.collisionHandler();
        this.doActions();
        this.walking();
        this.prevPos.x = this.pos.x;
        this.prevPos.y = this.pos.y;
        applyIncrement(this.speed, this.a, dt);
        applyIncrement(this.pos, this.speed, dt);
    }
    doActions() {
        this.actions.forEach((action)=> {
            action();
        });
        this.actions = [];
    }
    render(ctx, dy) {
        const x = (this.pos.x - this.size.x / 2) * this.scale;
        const y = ctx.canvas.height - ((this.pos.y) * this.scale);
        ctx.fillStyle = 'yellowgreen';
        ctx.fillRect(x, y + dy, this.size.x * this.scale, -this.size.y * this.scale);
        // ctx.beginPath();
        // ctx.arc(x + this.size.x * this.scale / 2, y + dy - this.size.y / 4 * 3 * this.scale,
        //     this.size.y / 4 * this.scale, 0, Math.PI * 2);
        // ctx.closePath();
        // ctx.fill();
    }
}