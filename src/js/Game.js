import {BaseClass} from "./BaseClass";
import {Player} from "./Player";
import {randomOfProbability, getProbabilityObj, Vec2} from "./lib";
import {Block} from "./Block";
import {DisappearingBlock} from "./DisappearingBlock";
import {HMoveBlock} from "./HMoveBlock";
import {VMoveBlock} from "./VMoveBlock";

const playerWidth = .25;
const playerHeight = .25;

export class Game extends BaseClass {
    constructor(canvas) {
        super();

        this.canvas = canvas;
        this.time = {
            currentTime: new Date().getTime(),
            dt: null,
        };
        this.globalDy = null;
        this.nextBlockY = null;
        this.score = 0;
        this.record = localStorage.getItem('record') || 0;
        this.blockTypes = [
            getProbabilityObj(Block, 5),
            getProbabilityObj(DisappearingBlock, 2),
            getProbabilityObj(HMoveBlock, 1),
            getProbabilityObj(VMoveBlock, 1),
        ];
        this.updateCanvasSize();
        window.addEventListener('resize', this.updateCanvasSize.bind(this));

        this.newGame();
    }
    checkRecord() {
        this.record = Math.max(this.score, this.record);
        localStorage.setItem('record', this.record.toString())
    }
    moveCamera() {
        this.items.player.forEach((player) => {
            this.globalDy = Math.max(this.globalDy, player.pos.y - player.worldSize.y / 2);
            this.score = Math.floor(this.globalDy * 10);
        });
    }
    updateTime() {
        const currentTime = new Date().getTime();
        this.time.dt = currentTime - this.time.currentTime;
        this.time.currentTime = currentTime;
    }
    addPlayer() {
        this.items.player.push(
            new Player({
                pos: new Vec2(playerWidth / 2, 0.1),
                size: new Vec2( playerWidth, playerHeight),
                worldSize: new Vec2(this.canvas.width / this.scale, this.canvas.height / this.scale),
            })
        )
    }
    addBlock(opt) {
        const BlockType = randomOfProbability(this.blockTypes);
        this.items.blocks.push(
            new BlockType({
                pos: new Vec2( opt.pos.x, opt.pos.y),
                size: new Vec2( opt.size.x, opt.size.y),
            }),
        );
    }
    addEarth() {
        this.items.blocks.push(
            new Block({
                pos: new Vec2(this.canvas.width / 2 / this.scale, 0.1 ),
                size: new Vec2(this.canvas.width / this.scale + playerWidth, .1 ),
            }),
        );
    }
    addBlocks() {
        if (this.nextBlockY < this.canvas.height / this.scale + this.globalDy) {
            this.addBlock({
                pos: new Vec2(Math.random() * this.canvas.width / this.scale, this.nextBlockY),
                size: new Vec2(.5, .1)
            });
            this.nextBlockY += Math.random() * .2 + .3;
            if (Math.random() < .1) this.nextBlockY += Math.random() * .2 + .3;
        }
    }
    updateCanvasSize() {
        this.canvas.width = window.innerHeight;
        this.canvas.height = window.innerHeight;
    }
    collisionsHandler() {
        this.items.player.forEach((player)=> {
            player.isOnSurface = false;
            if (player.speed.y > 0) return;
            this.items.blocks.forEach((block, index)=> {
                if (player.pos.y < block.pos.y && player.prevPos.y >= block.pos.y) {
                    const dy = (player.prevPos.y - block.pos.y) / (player.prevPos.y - player.pos.y);
                    const x = player.prevPos.x + dy * (player.prevPos.x - player.pos.x);
                    const size = block.size.x / 2 + player.size.x / 2;
                    if (
                        x > block.pos.x - size
                        && x < block.pos.x + size
                    ) {
                        player.speed.y = 0;
                        player.pos.y = block.pos.y;
                        player.isOnSurface = true;
                        player.jump();
                        this.blockAction(block.actionType, index)
                    }
                }
            });
        })
    }
    blockAction(type, index) {
        if (type === 'delete') {
            this.items.blocks.splice(index, 1)
        }
    }
    garbageCollector() {
        this.items.blocks.forEach((block, index) => {
            if (block.pos.y - this.globalDy < -1) {
                this.items.blocks.splice(index, 1);
            }
        })
    }
    gameOverCheck() {
        this.items.player.forEach((player) => {
            if (player.pos.y - this.globalDy < -0.01) {
                this.newGame();
            }
        })
    }
    newGame() {
        this.globalDy = 0;
        this.nextBlockY = 0.5;
        this.items.player = [];
        this.items.blocks = [];
        this.addPlayer();
        this.addEarth();
    }
    nextTick() {
        this.updateTime();
        this.moveCamera();
        this.checkRecord();
        this.addBlocks();
        super.nextTick(this.time.dt);
        this.garbageCollector();
        this.collisionsHandler();
        this.gameOverCheck();
    }
    render(ctx) {
        super.render(ctx, this.globalDy * this.scale);
        ctx.fillStyle = 'red';
        ctx.font = '20pt Arial';
        ctx.textAlign = 'left';
        ctx.fillText('record: ' + this.record, 10, 30);
        ctx.textAlign = 'center';
        ctx.fillText('total: ' + this.score, ctx.canvas.width / 2, 30)
    }
}