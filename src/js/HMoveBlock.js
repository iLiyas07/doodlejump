import {MoveBlock} from "./MoveBlock";
import {Vec2} from "./lib";

export class HMoveBlock extends MoveBlock{
    constructor(opt) {
        super(opt, new Vec2(1, 0));
        this.color = 'pink'
    }
}