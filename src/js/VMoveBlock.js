import {MoveBlock} from "./MoveBlock";
import {Vec2} from "./lib";

export class VMoveBlock extends MoveBlock{
    constructor(opt) {
        super(opt, new Vec2(0, 1));
        this.color = 'skyblue';
    }
}