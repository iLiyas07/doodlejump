import {Block} from "./Block";
import {applyIncrement, Vec2, vec2Multiply, vec2Sum} from "./lib";

export class MoveBlock extends Block {
    constructor(opt, direction) {
        super(opt);
        this._pos = opt.pos;
        this.range = new Vec2(Math.random()* .5 + .5, Math.random() * .5 + .5);
        this.translate = new Vec2((Math.random() - .5) * this.range.x * 1.5, Math.random() * this.range.y);
        this.direction = direction;
        this.speed = new Vec2(Math.random() * .3 + .3, Math.random() * .3 + .3);
        this.color = 'orange';
        this.pos = vec2Sum(this._pos, this.translate);
    }
    move(dt) {
        applyIncrement(this.translate, vec2Multiply(this.speed, this.direction), dt);
        if (Math.abs(this.translate.x) >= this.range.x) {
            this.direction.x *= -1;
        }
        if (Math.abs(this.translate.y) >= this.range.y) {
            this.direction.y *= -1;
        }
        this.pos = vec2Sum(this._pos, this.translate);
    }
    nextTick(dt) {
        this.move(dt);
    }
}